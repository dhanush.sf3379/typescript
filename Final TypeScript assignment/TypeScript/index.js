var currentUser;
var User = /** @class */ (function () {
    function User(paraName, paraAge, paraPhoneNumber) {
        this.userIdSeed = 1000;
        this.userId = this.userIdSeed++;
        this.userName = paraName;
        this.age = paraAge;
        this.phoneNumber = paraPhoneNumber;
    }
    return User;
}());
var user1 = new User("Dhanush", 22, 8667614055);
var userArrayList = [];
userArrayList.push(user1);
var Medicine = /** @class */ (function () {
    function Medicine(paraMedicineId, paraMedicineName, paraMedicineCount, paraMedicinePrice) {
        this.medicineId = paraMedicineId;
        this.medicineName = paraMedicineName;
        this.medicineCount = paraMedicineCount;
        this.medicinePrice = paraMedicinePrice;
    }
    return Medicine;
}());
// MedicineList
// 1) Paracetomol
// 2) Colpal
// 3) Stepsil
// 4) Iodex
// 5) Acetherol
//Creating Objects for medicine
var Paracetomol = new Medicine(1, "Paracetomol", 5, 2);
var Colpal = new Medicine(2, "Colpal", 5, 6);
var Stepsil = new Medicine(3, "Stepsil", 5, 10);
var Iodex = new Medicine(4, "Iodex", 5, 8);
var Acetherol = new Medicine(5, "Acetherol", 5, 15);
//Medicine Array List
var medicineArrayList = [];
//Pushing Object into Medicine Array List 
medicineArrayList.push(Paracetomol);
medicineArrayList.push(Colpal);
medicineArrayList.push(Stepsil);
medicineArrayList.push(Iodex);
medicineArrayList.push(Acetherol);
var Order = /** @class */ (function () {
    function Order(paraOrderedMedicineId, paraOrderedUserId, paraMedicineName, paraCount) {
        this.orderId = Order.orderIdSeed++;
        this.orderedMedicineId = paraOrderedMedicineId;
        this.orederedUserId = paraOrderedUserId;
        this.medicineName = paraMedicineName;
        this.count = paraCount;
    }
    Order.orderIdSeed = 100;
    return Order;
}());
var orderList = [];
var userLoginpage = document.getElementById("user-login");
var newUserPage = document.getElementById("new-user");
var existingUserPage = document.getElementById("existing-user");
var availableUser = document.getElementById("available-user");
var medicalItemListPage = document.getElementById("medicine-purchase");
var medicineCheck = document.getElementById("medicine-checklabel");
var medicineHistory = document.getElementById("medicine-historylabel");
newUserPage.style.display = "none";
existingUserPage.style.display = "none";
medicalItemListPage.style.display = "none";
function ExistingUser() {
    userLoginpage.style.display = "none";
    existingUserPage.style.display = "block";
    for (var i = 0; i < userArrayList.length; i++) {
        availableUser.innerHTML += "<h2>available user</h2>\n<p>User Id: " + userArrayList[i].userId + "|\n                                    User name: " + userArrayList[i].userName + "</p>";
    }
}
function Login() {
    currentUser = parseInt(document.getElementById("userId").value);
    for (var i = 0; i < userArrayList.length; i++) {
        if (userArrayList[i].userId == currentUser) {
            existingUserPage.style.display = "none";
            medicalItemListPage.style.display = "block";
        }
    }
}
function Check() {
    var selectedMedicine = document.getElementById("medicine-list").selectedIndex;
    // console.log(selectedMedicine);
    // console.log(typeof selectedMedicine)
    for (var i = 0; i < medicineArrayList.length; i++) {
        if (medicineArrayList[i].medicineId == selectedMedicine + 1) {
            medicineCheck.style.display = "block";
            medicineCheck.innerHTML = "<p>Medicine Id :" + medicineArrayList[i].medicineId + "</p>\n                                        <p> Medicine Name :" + medicineArrayList[i].medicineName + "</p>\n                                        <p> Medicine Count :" + medicineArrayList[i].medicineCount + "</p>\n                                        <p> Medicine Price :" + medicineArrayList[i].medicinePrice + "</p>";
        }
    }
}
function BuyItem() {
    // let continue1:boolean=true;
    var selectedMedicine = document.getElementById("medicine-list").selectedIndex;
    var medicineCount = parseInt(document.getElementById("count").value);
    for (var i = 0; i < medicineArrayList.length; i++) {
        if (medicineArrayList[i].medicineId == selectedMedicine + 1) {
            if (medicineArrayList[i].medicineCount > 0) {
                if (medicineArrayList[i].medicineCount - medicineCount >= 0) {
                    medicineArrayList[i].medicineCount -= medicineCount;
                    alert("item successfully purchased");
                    var order1 = new Order(medicineArrayList[i].medicineId, currentUser, medicineArrayList[i].medicineName, medicineCount);
                    orderList.push(order1);
                    // if(confirm("do you want to continue")){
                    // }
                }
                else {
                    alert("we have only " + medicineArrayList[i].medicineCount + " items");
                }
            }
            else {
                alert("out of stock");
            }
        }
    }
    Check();
}
function Exit() {
    medicalItemListPage.style.display = "none";
    userLoginpage.style.display = "block";
}
function ShowHistory() {
    medicineHistory.style.display = "block";
    if (orderList.length > 0) {
        for (var i = 0; i < orderList.length; i++) {
            if (orderList[i].orederedUserId == currentUser) {
                medicineHistory.innerHTML += "<p>Order Id : " + orderList[i].orderId + " |\n                                            Medicine Id : " + orderList[i].orderedMedicineId + " |\n                                            User Id : " + orderList[i].orederedUserId + " |\n                                            Medicine Name : " + orderList[i].medicineName + " |\n                                            Count: " + orderList[i].count + "<br>";
            }
        }
    }
    else {
        medicineHistory.innerHTML = 'no history';
    }
}
