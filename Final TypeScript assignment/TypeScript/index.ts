let currentUser:number;
class User{
    userIdSeed:number=1000;
    userId:number;
    userName:string;
    age:number;
    phoneNumber:number;
    constructor(paraName:string,paraAge:number,paraPhoneNumber:number){
        this.userId=this.userIdSeed++;
        this.userName=paraName;
        this.age=paraAge;
        this.phoneNumber=paraPhoneNumber;
    }
}

let user1=new User("Dhanush",22,8667614055);

let userArrayList:Array<User>=[];

userArrayList.push(user1);




class Medicine{
    medicineId:number;
    medicineName:string;
    medicineCount:number;
    medicinePrice:number;
    constructor(paraMedicineId:number,paraMedicineName:string,paraMedicineCount:number,paraMedicinePrice:number){
        this.medicineId=paraMedicineId;
        this.medicineName=paraMedicineName
        this.medicineCount=paraMedicineCount;
        this.medicinePrice=paraMedicinePrice;
    }
}
// MedicineList
// 1) Paracetomol
// 2) Colpal
// 3) Stepsil
// 4) Iodex
// 5) Acetherol


//Creating Objects for medicine

var Paracetomol=new Medicine(1,"Paracetomol",5,2);
var Colpal=new Medicine(2,"Colpal",5,6);
var Stepsil=new Medicine(3,"Stepsil",5,10);
var Iodex=new Medicine(4,"Iodex",5,8);
var Acetherol=new Medicine(5,"Acetherol",5,15);

//Medicine Array List

let medicineArrayList:Array<Medicine>=[];

//Pushing Object into Medicine Array List 

medicineArrayList.push(Paracetomol);
medicineArrayList.push(Colpal);
medicineArrayList.push(Stepsil);
medicineArrayList.push(Iodex);
medicineArrayList.push(Acetherol);





class Order{
    static orderIdSeed:number=100;
    orderId:number;
    orderedMedicineId:number;
    orederedUserId:number;
    medicineName:string;
    count:number;
    constructor(paraOrderedMedicineId:number,paraOrderedUserId:number,paraMedicineName:string,paraCount:number){
        this.orderId=Order.orderIdSeed++;
        this.orderedMedicineId=paraOrderedMedicineId;
        this.orederedUserId=paraOrderedUserId;
        this.medicineName=paraMedicineName;
        this.count=paraCount;
    }
}

let orderList:Array<Order>=[];

let userLoginpage=(document.getElementById("user-login")as HTMLDivElement);
let newUserPage=(document.getElementById("new-user")as HTMLDivElement);
let existingUserPage=(document.getElementById("existing-user")as HTMLDivElement);
let availableUser=(document.getElementById("available-user")as HTMLLabelElement);
let medicalItemListPage=(document.getElementById("medicine-purchase")as HTMLDivElement);
let medicineCheck=(document.getElementById("medicine-checklabel")as HTMLLabelElement);
let medicineHistory=(document.getElementById("medicine-historylabel")as HTMLLabelElement);

newUserPage.style.display="none";
existingUserPage.style.display="none";
medicalItemListPage.style.display="none";



function ExistingUser(){

    userLoginpage.style.display="none";
    existingUserPage.style.display="block";

    for(let i=0;i<userArrayList.length;i++){
        availableUser.innerHTML+=`<h2>available user</h2>\n<p>User Id: ${userArrayList[i].userId}|
                                    User name: ${userArrayList[i].userName}</p>`;
    }

    
}

function Login(){

    currentUser=parseInt((document.getElementById("userId")as HTMLInputElement).value);
    
    for(let i=0;i<userArrayList.length;i++){
        if(userArrayList[i].userId==currentUser){

            existingUserPage.style.display="none";
            medicalItemListPage.style.display="block";
        }
    }
}

function Check(){

    let selectedMedicine = (document.getElementById("medicine-list")as HTMLSelectElement).selectedIndex;
   
    // console.log(selectedMedicine);
    // console.log(typeof selectedMedicine)


    for(let i=0;i<medicineArrayList.length;i++){

        if(medicineArrayList[i].medicineId==selectedMedicine+1){
            medicineCheck.style.display="block";
            medicineCheck.innerHTML=`<p>Medicine Id :${medicineArrayList[i].medicineId}</p>
                                        <p> Medicine Name :${medicineArrayList[i].medicineName}</p>
                                        <p> Medicine Count :${medicineArrayList[i].medicineCount}</p>
                                        <p> Medicine Price :${medicineArrayList[i].medicinePrice}</p>`;
            
        }
    }
}

function BuyItem(){
    // let continue1:boolean=true;
    let selectedMedicine = (document.getElementById("medicine-list")as HTMLSelectElement).selectedIndex;
    let medicineCount=parseInt((document.getElementById("count")as HTMLInputElement).value);
    
    for(let i=0;i<medicineArrayList.length;i++){

        if(medicineArrayList[i].medicineId==selectedMedicine+1){

            if(medicineArrayList[i].medicineCount>0){
                if(medicineArrayList[i].medicineCount-medicineCount>=0){
                    medicineArrayList[i].medicineCount-=medicineCount;
                    alert("item successfully purchased");
                    var order1=new Order(medicineArrayList[i].medicineId,currentUser,medicineArrayList[i].medicineName,medicineCount);
                    orderList.push(order1);

                    // if(confirm("do you want to continue")){

                       
                        
                    // }

                }
                else{
                    alert(`we have only ${medicineArrayList[i].medicineCount} items`);
                }
               
            }
            else{
                alert(`out of stock`);
            }
           
        }
    }
    Check();
   
}
function Exit(){
    medicalItemListPage.style.display="none";
    userLoginpage.style.display="block";

}
function ShowHistory(){

    medicineHistory.style.display="block";
    if(orderList.length>0){
        for(let i=0;i<orderList.length;i++){
            if(orderList[i].orederedUserId==currentUser){

                medicineHistory.innerHTML+=`<p>Order Id : ${orderList[i].orderId} |
                                            Medicine Id : ${orderList[i].orderedMedicineId} |
                                            User Id : ${orderList[i].orederedUserId} |
                                            Medicine Name : ${orderList[i].medicineName} |
                                            Count: ${orderList[i].count}<br>`;

            }   

        } 

    }
    else{
        medicineHistory.innerHTML='no history';
    }



}

